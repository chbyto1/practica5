package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;

import java.util.Iterator;

public class Practica5 extends ApplicationAdapter implements ApplicationListener,InputProcessor,GestureDetector.GestureListener{

	SpriteBatch batch;
	Texture img;
	Texture background;
	Sprite sprite;
	TextureAtlas texAtlas;
	private OrthographicCamera camera;
	float stateTime;
	float currentFrame;
	String currentAtlasKey;
	private float elapsedTime = 0;
	private Rectangle player;
	Animation animation;
	public Animation<TextureRegion> runningAnimation;
	private Sound motor;
	private Sound accel;
	boolean on;

	static float accelerate = 1;
	static final float slow = -10;
	static final float speed = 0.5f;

	GestureDetector gd;

	//runningAnimation = new Animation<TextureRegion>(0.033f,atlas.findRegions("animation_sheet.png"),Animation.PlayMode.Looping);
	@Override
	public void create () {
		batch = new SpriteBatch();

		gd = new GestureDetector(this);
		InputMultiplexer im = new InputMultiplexer();

		im.addProcessor(gd);
		im.addProcessor(this);

		Gdx.input.setInputProcessor(im);



		img = new Texture("badlogic.jpg");
		background = new Texture("wally.jpg");


		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);

		player = new Rectangle();
		player.x = 800 / 2 - 128 / 2;
		player.y = 128;
		player.width = 128;
		player.height = 128;

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		elapsedTime += Gdx.graphics.getDeltaTime();
		//batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), player.x, player.y);
		batch.draw(background,0,0);
		batch.setProjectionMatrix(camera.combined);
		batch.end();

		if(Gdx.input.isTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			player.x = touchPos.x - 256/2;
			player.y = touchPos.y - 128/2;

		}
		if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT))
		{
			startEngine();
			if(on == false) // parar el motor
			{
				stopEngine();
			}
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
		{
			if(on == true)
			{
				accelerate();
			}
		}

		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) player.x -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) player.x += 200 * Gdx.graphics.getDeltaTime();

		if(player.x < 0) player.x = 0;
		if(player.y <0 ) player.y = 0;
		if(player.y >380) player.y = 380;
		if(player.x > 640 - 128) player.x = 640 - 128;


	}
	public void accelerate()
	{
		long soundID = accel.play();
		float accelerate2;
		accelerate2 = accelerate += speed * Gdx.graphics.getDeltaTime();
		accel.setPitch(soundID,accelerate2);

		accel.play();
	}
	public void stopEngine()
	{
		motor.stop();
	}
	public void startEngine()
	{
		motor.play();
		on = true;
	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		texAtlas.dispose();
		motor.dispose();
		accel.dispose();
		background.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		camera.translate(-deltaX,deltaY,0);
		camera.update();
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {

	}
}

